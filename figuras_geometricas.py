class Coordenada:
    def __init__(self, x, y,z):
        self.__x = x
        self.__y = y
        self.__z = z
        
    def setX (self,x):
        self.__x = x

    def getX(self):
        return self.__x

    def setY (self,y):
        self.__y = y

    def getY(self):
        return self.__y

    def setZ (self,z):
        self.__z = z

    def getZ(self):
        return self.__z

    def __str__(self):
        return "(" + str (self.__x) + ", " + str(self.__y) + ", " + str(self.__z) + ")"

class Cuadrado:
    def __init__(self, pAltura, coordenada):
        self.setAltura(pAltura)
        self.setCoordenada(coordenada)

    def calculoCuadrado(self):
        return "La superficie del cuadrado es: " + str(self.__altura * self.__altura)

    def setAltura (self, pAltura):
        if type(pAltura) is float or type(pAltura) is int:
            if pAltura <= 0:
                print("Ingrese un número positivo")
            else:
                self.__altura = pAltura
        else:
            print("Ingrese un número")

    def getAltura(self):
        return "La altura del cuadrado es: " + str(self.__altura)


    def setCoordenada (self, coordenada):
        self.__coordenada = coordenada

    def getCoordenada (self):
        return self.__coordenada

    def __str__(self):
        return "["+self.altura+","+str(self.coordenada)+"]"
        

class Triangulo:

    def __init__(self,pAltura,pBase,coordenada):
        self.setAltura(pAltura)
        self.setBase(pBase)
        self.setCoordenada(coordenada)
    
    def CalculoTriangulo(self):
        return "La superficie del triangulo es: " +str(self.__altura *self.__base/2)

    def setAltura (self, pAltura):
        if type(pAltura) is float or type(pAltura) is int:
            if(pAltura) <=0:
                print ("Ingrese un número positivo")
            else:
                self.__altura =pAltura
        else:
            print("Ingrese un número")

    def getAltura(self):
        return ("La altura del triangulo es: " + str(self.__altura))
    

    def setBase (self, pBase):
        if type(pBase) is float or type(pBase) is int:
            if(pBase) <=0:
                print ("Ingrese un número positivo")
            else:
                self.__base=pBase
        else:
            print("Ingrese un número")

    def getBase(self):
        return ("La base del triangulo es: " + str(self.__base))


    def setCoordenada (self, coordenada):
        self.__coordenada = coordenada

    def getCoordenada (self):
        return self.__coordenada
        
    
from math import pi

class Circulo: 
        
    def calculoCirculo (self):
        return (pi*(self.__radio*self.__radio))

    def setRadio(self, pRadio):
        if type(pRadio) is float or type(pRadio) is int:
            if pRadio <=0:
                print("Escribe un número positivo")
            else:
                self.__radio =pRadio
        else:
            print("Escribe un número")

    def getRadio(self):
        return ("El radio del circulo es: " +str(self.__radio))

    def setCoordenada (self, coordenada):
        self.__coordenada = coordenada

    def getCoordenada (self):
        return self.__coordenada

    def __init__(self, pRadio, coordenada):
        self.setRadio(pRadio)
        self.setCoordenada(coordenada)
        
class Rectangulo:
    def __init__(self,pBase,pAltura,coordenada):
        self.setBase(pBase)
        self.setAltura(pAltura)
        self.setCoordenada(coordenada)
    def setBase(self,pBase):
        if type(pBase) is float or type(pBase) is int:
            if pBase <=0:
                print("Escribe un número positivo")
            else:
                self.__base =pBase
        else:
            print("Escribe un número")
            
    def setAltura(self,pAltura):
        if type(pAltura) is float or type(pAltura) is int:
            if pAltura <=0:
                print("Escribe un número positivo")
            else:
                self.__altura =pAltura
        else:
            print("Escribe un número")
            
    def setCoordenada (self, coordenada):
        self.__coordenada = coordenada    
    
    def calculoRectangulo(self):
        return self.__base*self.__altura
    
    def getCoordenada (self):
        return self.__coordenada
    def getBase(self):
        return ("La base del Rectángulo es: " + str(self.__base))
    def getAltura(self):
        return ("La altura del Rectángulo es: " + str(self.__altura))
    
    
